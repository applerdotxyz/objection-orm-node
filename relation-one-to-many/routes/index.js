const router = require("express").Router();
const Owner = require('../models/Owner');
const Car = require('../models/Car');

router.get('/owners', async (req, res) => {
    const owners = await Owner.query().withGraphFetched('cars');
    res.json(owners);
})

router.get('/cars', async (req, res) => {
    const cars = await Car.query().withGraphFetched('owner');
    res.json(cars);
})

router.use((req, res) => res.send('Hello from Objection.js'));

module.exports = router;