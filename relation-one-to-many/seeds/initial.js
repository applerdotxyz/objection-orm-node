
exports.seed = async function(knex) {
    await knex('owner').del()
    await knex('car').del()

    const firstId = await knex('owner').insert({
        name: 'rajesh'
    }).then(row => row[0]);

    const secondId = await knex('owner').insert({
        name: 'suresh'
    }).then(row => row[0]);

    const carOne = await knex('car').insert([{
        company: 'Audi',
        model: 'A6',
        owner_id: firstId
    },{
        company: 'Audi',
        model: 'Q8',
        owner_id: secondId
    },{
        company: 'BMW',
        model: 'X5',
        owner_id: secondId
    },{
        company: 'BMW',
        model: 'Z4',
        owner_id: secondId
    }])

    return;
};
