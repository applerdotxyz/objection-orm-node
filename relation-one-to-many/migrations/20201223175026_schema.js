
exports.up = async function(knex) {
  await knex.schema.createTable('owner', (table) => {
    table.increments()
    table.string('name')
  })

  await knex.schema.createTable('car', (table) => {
    table.increments()
    table.string('company')
    table.string('model')
    table.integer('owner_id').unsigned().references('id').inTable('owner')
  })

  return;
};

exports.down = async function(knex) {
  await knex.dropTableIfExists('owner');
  await knex.dropTableIfExists('car');
  return;
};
