const { Model } = require("objection");

class Owner extends Model{
    static get tableName() {
        return 'owner';
    }

    static get relationMappings() {
        const Car = require('./Car');
        return {
            cars: {
                modelClass: Car,
                relation: Model.HasManyRelation,
                join: {
                    from: 'owner.id',
                    to: 'car.owner_id'
                }
            }
        }
    }
}

module.exports = Owner;