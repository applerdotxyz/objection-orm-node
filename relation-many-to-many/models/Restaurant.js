const { Model } = require("objection");

class Restaurant extends Model{
    static get tableName() {
        return 'restaurant';
    }

    static get relationMappings() {
        const Person = require('./Person');
        return {
            owners: {
                modelClass: Person,
                relation: Model.ManyToManyRelation,
                join: {
                    from: 'restaurant.id',
                    through: {
                        from: 'person_restaurant.restaurant_id',
                        to: 'person_restaurant.owner_id'
                    },
                    to: 'person.id'
                }
            }
        }
    }
}

module.exports = Restaurant;