const { Model } = require("objection");

class Person extends Model{
    static get tableName() {
        return 'person';
    }

    static get relationMappings() {
        const Restaurant = require('./Restaurant');
        return {
            restaurants: {
                modelClass: Restaurant,
                relation: Model.ManyToManyRelation,
                join: {
                    from: 'person.id',
                    through: {
                        from: 'person_restaurant.owner_id',
                        to: 'person_restaurant.restaurant_id'
                    },
                    to: 'restaurant.id'
                }
            }
        }
    }
}

module.exports = Person;