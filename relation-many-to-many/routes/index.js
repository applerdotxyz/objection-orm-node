const router = require("express").Router();
const Person = require('../models/Person');
const Restaurant = require('../models/Restaurant');

router.get('/persons', async (req, res) => {
    const persons = await Person.query().withGraphFetched('restaurants');
    res.json(persons);
})

router.get('/restaurants', async (req, res) => {
    const restaurants = await Restaurant.query().withGraphFetched('owners');
    res.json(restaurants);
})

router.use((req, res) => res.send('Hello from Objection.js'));

module.exports = router;