
exports.seed = async function(knex) {
    await knex('person').del()
    await knex('restaurant').del()

    const firstId = await knex('person').insert({
        name: 'Ram'
    }).then(row => row[0]);

    const secondId = await knex('person').insert({
        name: 'Shyam'
    }).then(row => row[0]);

    const thirdId = await knex('person').insert({
        name: 'Manish'
    }).then(row => row[0]);

    const restaurantOne = await knex('restaurant').insert({
        name: 'Veggies center',
        location: 'Delhi'
    }).then(row => row[0]);

    const restaurantTwo = await knex('restaurant').insert({
        name: 'Hungry Birds',
        location: 'Mumbai'
    }).then(row => row[0]);

    const person_restaurant = await knex('person_restaurant').insert([{
        owner_id: firstId,
        restaurant_id: restaurantOne
    },{
        owner_id: secondId,
        restaurant_id: restaurantOne
    },{
        owner_id: thirdId,
        restaurant_id: restaurantTwo
    },{
        owner_id: thirdId,
        restaurant_id: restaurantOne
    },{
        owner_id: firstId,
        restaurant_id: restaurantTwo
    },])

    return;
};
